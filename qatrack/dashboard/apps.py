from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'qatrack.dashboard'
    verbose_name="Dashboard"
